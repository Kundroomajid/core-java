import java.util.Scanner;
/**
 * This is insertion Sort Java iv Sem MCA
 * @version 1.01 13/04/2018
 * @author Kundroo Majid
 */

class InsertionSort
{
    Scanner in = new Scanner(System.in);
    private int n = 0;
    private int [] nums = new int[10];

    int setMax()
    {
	System.out.print(" Enter max num of elements to be stored :");
	n = in.nextInt();
	return n; 
    }

    int[] setNums()
    {
	for(int i=0;i<n;i++)
	    {
		System.out.printf("Enter the element [ %d of %d ]  : ", i+1,n);
		this.nums[i] = in.nextInt();
		
	    }
	return nums;
    }

    void getNums()
    {

	for(int i=0;i<n;i++)
	    {
		System.out.printf("%d ",nums[i]);
	    }
    }

    void insertionSortAlgo()
    {
	//insertion sort
	for(int i = 1;i < n;i++)
	    {
		int  value=nums[i];
		int  hole = i;
		while(hole > 0 && nums[hole-1]>value)
		    {
			nums[hole] = nums[hole-1];
			hole--;
		    }
		nums[hole] = value;
	    }
    }
}

public class InsertionSortTest{
    public static void main(String [] args)
    {
	InsertionSort is = new InsertionSort();
	is.setMax();
	is.setNums();
	System.out.printf("The entered elements are :\n");
	is.getNums();
	is.insertionSortAlgo();
	System.out.printf("\nThe Elements after Insertion sort are :\n");
	is.getNums();

    }
}
    
   