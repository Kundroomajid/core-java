import java.util.Scanner;
/**
 * This is palindrome Java iv Sem MCA
 * @version 1.01 13/04/2018
 * @author Kundroo Majid
 */
public class BubbleSort{
    public static void main(String[] args){
	Scanner in = new Scanner(System.in);
	int list [] = new int[10];
	System.out.print("Enter the max number of elements to be stored :");
	int n = in.nextInt();
	
	for(int i=0;i<n;i++)
	    {
		System.out.print("Enter the element :");
		list[i] = in.nextInt();
	    }
	System.out.println("The Entered elements are ");
	for (int i =0; i< n; i++)
	    {
		System.out.print(list[i]);
	    }

	//bubble sort
	for(int i=0;i<n;i++)
	    {
		for (int j =0;j<n;j++)
		    {
			if (list[i] < list[j])
			    {
				int temp = list[i];
				list[i] = list[j];
				list[j] = temp;
			    }
		    }
	    }
	System.out.printf("\nPerforming bubble sort : \n");

	System.out.println("The elements after sort are  ");
	for (int i =0; i< n; i++)
	    {
		System.out.print(list[i]);
	    }


    }
}
	