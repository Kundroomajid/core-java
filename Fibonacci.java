/**
2 * Fibonacci in Java 
3 * @version 1.01 09/04/2018
4 * @author Kundroo Majid
5 */

import java.util.Scanner;
public class Fibonacci{
	public static void main(String [] args) {
		Scanner in = new Scanner(System.in);
		System.out.println("Enter the number of elements of Fibonacci you want to print");
		int num = in.nextInt();
		int a = 0;
		int b = 1;
		System.out.println("The Fibonacci series generated is");
		System.out.printf("%d ",a);
		while( num >= 2)
		{
			System.out.printf("%d ", b);
			int old_b = b;
			b = a + b;
			a = old_b;
			num--;
		}
	}
}
		
		