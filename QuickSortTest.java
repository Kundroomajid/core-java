import java.util.Scanner;
/**
 * This is Quick Sort Java iv Sem MCA
 * @version 1.01 13/04/2018
 * @author Kundroo Majid
 */

class QuickSort
{
    Scanner in = new Scanner(System.in);
    int n = 0;
    int [] nums = new int[10];


    int setMax()
    {
	System.out.print(" Enter max num of elements to be stored :");
	n = in.nextInt();
	return n; 
    }


    int[] setNums()
    {
	for(int i=0;i<n;i++)
	    {
		System.out.printf("Enter the element [ %d of %d ]  : ", i+1,n);
		this.nums[i] = in.nextInt();
	    }
	return nums;
    }


    void getNums()
    {

	for(int i=0;i<n;i++)
	    {
		System.out.printf("%d ",nums[i]);
	    }
    }



    void quickSort(int nums[], int p, int r)
    {
  
	if (p < r)
	    {
		int q = partition(nums,p,r);
		quickSort(nums,p,q-1);
		quickSort(nums,q+1,r);
	    }
    }

    int partition(int nums[], int p, int r)
    {
 
	int  x = nums[r];
	int i = p-1;
	for(int j= p; j< r-1;j++)
	    {
		if(nums[j] >= x)
		    {
			i = i+1;
			swap(nums,i,j);
		    }
	    }
	swap(nums,i+1,r);
	return i+1;
    }
    void swap(int nums[],int i, int j)
    {
	int p = nums[i];
	nums[i] = nums[j];
	nums[j] = p;
    }

}

public class QuickSortTest{
    public static void main(String [] args)
    {
	QuickSort qs = new QuickSort();
	qs.setMax();
	qs.setNums();
	System.out.printf("The entered elements are :\n");
	qs.getNums();
	qs.quickSort(qs.nums,0,qs.n-1);
	System.out.printf("\nThe Elements after Quick sort are :\n");
	qs.getNums();

    }
}
    
   