/**
 * This is Binary Search implementation
 * @version 1.01 13/04/2018
 * @author Kundroo Majid
 */
import java.util.Scanner;

class BinarySearch
{
    Scanner in = new Scanner(System.in);
    private int n;
    private int key;	
    private int nums[] = new int [10];

    int getMax()
    {
	System.out.print(" Enter max num of elements to be stored :");
	n = in.nextInt();
	return n; 
    }

    int[] getNums()
    {
	for(int i=0;i<n;i++)
	    {
		System.out.printf("Enter the element [ %d of %d ]  : ", i+1,n);
		this.nums[i] = in.nextInt();
		
	    }
	return nums;
    }
    int getKey()
    {
	System.out.print("Enter value to be searched :");
	key = in.nextInt();
	return key;
    }
    void  binarySearchAlgo()
    {
	int low = 0;
	int high = n -1;
	
	while(low <= high)
	    {
		int mid = (low + high )/2;
		if(nums[mid] == key)
		    {
			System.out.printf("Element found at  %d location", mid+1);	
			break;
		    }
		else if(key < mid)
		    {
			high = mid -1;
		    }
		else
		    {
			low = mid + 1;
		    }
	    }
	if (low > high) System.out.print("Element not found :");
    }
    //bubble sort
    void bubbleSort()
    {
	for(int i=0;i<n;i++)
	    {
		for (int j =0;j<n;j++)
		    {
			if (nums[i] < nums[j])
			    {
				int temp = nums[i];
				nums[i] = nums[j];
				nums[j] = temp;
			    }
		    }
	    }
    }
}
public class BinarySearchTest
{
    public static void main(String [] args)
    {
	
	BinarySearch bs = new BinarySearch();
	int x = bs.getMax();
	bs.getNums();
	bs.getKey();
	bs.bubbleSort();
	bs.binarySearchAlgo();
	  
    }
}

