import java.util.Scanner;

/**
 * Even Odd in Java
 * @version 1.01 13/04/2018
 * @author Kundroo Majid
 */

public class EvenOdd {
    public static void main(String [] args) 
    {
	Scanner in = new Scanner(System.in);
	System.out.print("Enter an element :");
	int num = in.nextInt();

	if( num % 2 == 0)
	    {
		System.out.printf("Entered element %d is even \n", num);
	    }
	else
	    {
		System.out.printf("Entered element %d is odd\n", num);
	    }
    }
}
