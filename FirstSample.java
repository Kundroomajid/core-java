/**
 * This is the first sample program in Java iv Sem MCA
 * @version 1.01 09/04/2018
 * @author Kundroo Majid
 */
public class FirstSample {
    // FirstSample is the name of the class
    public static void main(String [] args) {
        // main is the entry point of a program.
        System.out.println("Hello java");
        // used to print a message on console
    }
}
