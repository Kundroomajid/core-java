/**
 * Example to check a number is prime or not in  Java iv Sem MCA
 * @version 1.01 10/04/2018
 * @author Kundroo Majid
 */

import java.util.Scanner;

public class PrimeNumber
{
    public static void main(String [] args)
    {
	Scanner in = new Scanner(System.in);
	System.out.print("Enter a number to check if it is prime or not :");
	int num = in.nextInt();


	if( num == 0 || num == 1)
	    {
		System.out.printf("%d is not prime ", num);
	    }
	else
	    {
		for(int i = 2 ; i < num; i++)
		    {
			if( num % i == 0)
			    {
				System.out.printf("%d is not prime ", num);
				break;	
			    }
			else
			    {
				System.out.printf("%d is a prime number ", num);
				break;
			    }
		    }
	    }
    }
}
