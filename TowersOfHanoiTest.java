import java.util.Scanner;

/**
 * Towers of Hanoi  in Java
 * @version 1.01 13/04/2018
 * @author Kundroo Majid
 */

class TowerOfHanoi {
   
    Scanner in = new Scanner(System.in);
    int n =0;
    char x = 'x';
    char y = 'y';
    char z = 'z';


    void setDisks()
    {
	System.out.print("Enter number of disks :");
	n = in.nextInt();
    }

    void towerOfHanoi(int n, char x, char y, char z)
    {
	if( n>= 1)
	    {
		towerOfHanoi( n-1, x, z, y);
		System.out.printf("Move top disk from tower %c to tower %c\n",x,y);
		towerOfHanoi(n-1, z,y,x);
	    }
    }
	 
}

public class TowersOfHanoiTest{
    public static void main(String [] args) 
    {
	TowerOfHanoi toh = new TowerOfHanoi();
	toh.setDisks();
	toh.towerOfHanoi(toh.n,toh.x,toh.y,toh.z);
    }
}
	