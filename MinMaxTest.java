/**
 * Find Minimum and Maximum in an array.
 * @version 1.01 13/04/2018
 * @author Kundroo Majid
 */
import java.util.Scanner;

class MinMax
{
    Scanner in = new Scanner(System.in);
    private int n;
    private int nums[] = new int [10];


    int getMax()
    {
	System.out.print(" Enter max num of elements to be stored :");
	n = in.nextInt();
	return n; 
    }


    int[] getNums()
    {
	for(int i=0;i<n;i++)
	    {
		System.out.printf("Enter the element [ %d of %d ]  : ", i+1,n);
		this.nums[i] = in.nextInt();
	    }
	return nums;
    }


    int searchMinAlgo()
    {
	int min = nums[0]; 
	for(int i=0; i<n;i++)
	    {
	      	if (min > nums[i])
		    {
			min = nums[i];
		    }
	    }
	return min;
    }


    int searchMaxAlgo()
    {
	int max = nums[0]; 
	for(int i=0; i<n;i++)
	    {
		if (max < nums[i])
		    {
			max = nums[i];
		    }
	    }
	return max;
    }
}


public class MinMaxTest
{
    public static void main(String [] args)
    {
	MinMax mm = new MinMax();
	int x = mm.getMax();
	mm.getNums();
	System.out.println("The minimum of list is : " +mm.searchMinAlgo());
	System.out.println("The maximum of list is : " +mm.searchMaxAlgo());
    }
}

