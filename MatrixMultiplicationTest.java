/**
 * Multiplication of Matrices in  Java iv Sem MCA
 * @version 1.01 13/04/2018
 * @author Kundroo Majid
 */
import java.util.Scanner;
public class MatrixMultiplicationTest
{
    public static void main(String [] args)
    {
	MatrixAddition ma = new MatrixAddition();
	ma.setDimensions();
	System.out.printf("Enter the first matrix \n");
	ma.setMatrix1();
	System.out.printf("Enter the 2nd matrix \n");
	ma.setMatrix2();
	ma.multiplicationOfMatrices();

	System.out.println("The multiplication of matrices is ");
	for(int i=0;i<ma.rows1;i++)
	    {
		for(int j=0;j<ma.cols1;j++)
		    {
			System.out.printf("%3d", ma.result[i][j]);
		    }
		System.out.println(" ");
	    }
    }

}
class MatrixAddition
{

    Scanner in = new Scanner(System.in);

    // instance fields
    int [][] matrix1 = new int[10][10];
    int [][] matrix2 = new int[10][10];
    int [][] result = new int[10][10];
    int cols1;
    int rows1;
    int cols2;
    int rows2;
   

   

    void setDimensions()
    {
	System.out.print("Enter the rows and cols of first matrix :");
	cols1 = in.nextInt();
	rows1 = in.nextInt();
	System.out.print("Enter the rows and cols of 2nd matrix :");
	cols2 = in.nextInt();
	rows2 = in.nextInt();
    }

    void setMatrix1()
    {
	for(int i=0;i<rows1;i++)
	    {
		for(int j=0;j<cols1;j++)
		    {
			System.out.print("Enter the element :");
			matrix1[i][j] = in.nextInt();
		    }
	    }
    }

    void setMatrix2()
    {
	for(int i=0;i<rows2;i++)
	    {
		for(int j=0;j<cols2;j++)
		    {
			System.out.print("Enter the element :");
			matrix2[i][j] = in.nextInt();
		    }
	    }
    }


    void multiplicationOfMatrices()
    {
	int sum = 0;
	//code for multiplication
	for(int i=0;i<rows1;i++)
	    {
		for(int j=0;j<cols1;j++)
		    {
			for(int k=0;k<rows2;k++)
			    {
				sum= sum+matrix1[i][k] * matrix2[k][j];
			    }
			result[i][j] = sum;
			sum =0;
	  
		    }
	    }
	
    }
}