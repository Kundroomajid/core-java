import java.util.Scanner;
/**
 * This is palindrome Java iv Sem MCA
 * @version 1.01 13/04/2018
 * @author Kundroo Majid
 */
public class Palindrome {
    public static void main(String [] args) {
	Scanner in = new Scanner(System.in);
	System.out.print("Enter a number :");
	int num = in.nextInt();
	int originalint = num;
	int remainder = 0;
	int reversedinteger = 0;
	while( num !=0){
	    {
		remainder = num % 10;
		reversedinteger = reversedinteger *10 + remainder;
		num = num/10;
	    }
	   
	}
	if(originalint == reversedinteger)
	    {
		System.out.println("The given number is a palindrome");
	    }
	else
	    {
		System.out.println("The given number is a not a  palindrome");
	    }
		
    }
}