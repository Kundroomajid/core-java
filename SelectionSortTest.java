import java.util.Scanner;
/**
 * This is Selection Sort Java iv Sem MCA
 * @version 1.01 13/04/2018
 * @author Kundroo Majid
 */

class SelectionSort
{
    Scanner in = new Scanner(System.in);
    private int n = 0;
    private int [] nums = new int[10];

    int setMax()
    {
	System.out.print(" Enter max num of elements to be stored :");
	n = in.nextInt();
	return n; 
    }

    int[] setNums()
    {
	for(int i=0;i<n;i++)
	    {
		System.out.printf("Enter the element [ %d of %d ]  : ", i+1,n);
		this.nums[i] = in.nextInt();
		
	    }
	return nums;
    }

    void getNums()
    {

	for(int i=0;i<n;i++)
	    {
		System.out.printf("%d ",nums[i]);
	    }
    }

    void selectionSortAlgo()
    {
	for (int i=0;i<n;i++)
	    {
		int j = i;
		for (int k = i+1;k<n;k++)
		    {
			if(nums[k]<nums[j])
			    {
				j = k;
			    }
			int temp = nums[i];
			nums[i] = nums[j];
			nums[j] = temp;
		    }
	    }
    }
}

public class SelectionSortTest{
    public static void main(String [] args)
    {
	SelectionSort ss = new SelectionSort();
	ss.setMax();
	ss.setNums();
	System.out.printf("The entered elements are :\n");
	ss.getNums();
	ss.selectionSortAlgo();
	System.out.printf("\nThe Elements after Selection sort are :\n");
	ss.getNums();

    }
}
    
   