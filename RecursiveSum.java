import java.util.Scanner;

/**
 * Recursive sum of array
 * @version 1.01 13/04/2018
 * @author Kundroo Majid
 */

public class RecursiveSum {
    public static void main(String [] args) 
    {
	Sum s = new Sum();
	s.setNums();
	s.getMax();
	s.setNums();
	int res = s.rSum(s.nums,s.n);
	System.out.println("The sum is :"+res);
    }
}

class Sum
{
    int [] nums = new int[10];
    int n = 0;
    Scanner in = new Scanner(System.in);

    void getMax()
    {    System.out.println("Enter the max no of elements to be stored :");
	n = in.nextInt();
    }
    void setNums()
    {
	for(int i = 0; i< n;i++)
	    {
		System.out.print("Enter the element :");
		nums[i] = in.nextInt();
	    }
    }

    int rSum(int []a , int n)
    {
	if (n <= 0)
	    return 0;
	else
	    return rSum(a, n-1) + a[n];
    }
}