/**
* This is Linear Search implementation from Core jav
* @version 1.01 11/04/2018
* @author Kundroo Majid
*/
import java.util.Scanner;

class LinearSearch
{
	Scanner in = new Scanner(System.in);
	int n;
	int key;	
	int nums[] = new int [10];

int getMax()
     {
	System.out.print(" Enter max num of elements to be stored :");
	n = in.nextInt();
	return n; 
     }

int[] getNums()
    {
	for(int i=0;i<n;i++)
	    {
		System.out.printf("Enter the element [ %d of %d ]  : ", i+1,n);
		this.nums[i] = in.nextInt();
		
	    }
	return nums;
    }
    int getKey()
    {
	System.out.print("Enter value to be searched :");
	key = in.nextInt();
	return key;
    }
    int linearSearchAlgo()
    {
	int pos = 0;
	for (int i = 0; i<n; i++)
	    {
		if( key == nums[i])
		    {
		        pos = i+1;
			break;
		    }
		else
		    {
			pos = 0;
		    }
	    }
	return pos;
    }
}


public class LinearSearchTest{
    public static void main(String [] args)
    {
	LinearSearch ls = new LinearSearch();
	int x =	ls.getMax();
	ls.getNums();
	ls.getKey();
	int res = ls.linearSearchAlgo();
	if( res == 0)
	    {
		System.out.println("Elemet not found ");
	    }
	else
	    {
		System.out.println("Element found at location " +res);
	    }
    }
}