/**
 * Transpose of Matrices in  Java iv Sem MCA
 * @version 1.01 13/04/2018
 * @author Kundroo Majid
 */
import java.util.Scanner;
public class TransposeMatrixTest
{
    public static void main(String [] args)
    {
	TransposeMatrix tm = new TransposeMatrix();
	tm.setDimensions();
	System.out.printf("Enter the  matrix \n");
	tm.setMatrix();
	tm.transposeOfMatrices();

	System.out.println("The transpose of matrices is ");
	for(int i=0;i<tm.rows;i++)
	    {
		for(int j=0;j<tm.cols;j++)
		    {
			System.out.printf("%3d", tm.transposematrix[i][j]);
		    }
		System.out.println(" ");
	    }
    }
}


class TransposeMatrix
{

    Scanner in = new Scanner(System.in);

    // instance fields
    int [][] matrix = new int[10][10];
    int [][] transposematrix = new int[10][10];
    int cols;
    int rows;
    

 void setDimensions()
    {
	System.out.print("Enter the rows and cols of matrix :");
	cols = in.nextInt();
	rows = in.nextInt();
    }


    void setMatrix()
    {
	for(int i=0;i<rows;i++)
	    {
		for(int j=0;j<cols;j++)
		    {
			System.out.print("Enter the element :");
			matrix[i][j] = in.nextInt();
		    }
	    }
    }

  

    void transposeOfMatrices()
    {
	//code for transpose
	for(int i=0;i<rows;i++)
	    {
		for(int j=0;j<cols;j++)
		    {
			transposematrix[i][j] = matrix[j][i];
	  
		    }
	    }
	
    }
}